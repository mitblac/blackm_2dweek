﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class candy : MonoBehaviour
{

	public GameObject self;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnCollisionEnter2D(Collision collision)
	{
		//Check for a match with the specified name on any GameObject that collides with your GameObject
		if (collision.gameObject.tag == "Player")
		{
			//If the GameObject's name matches the one you suggest, output this message in the console
			self.gameObject.SetActive(false);
		}

	}
}