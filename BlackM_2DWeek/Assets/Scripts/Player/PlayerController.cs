﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

	Rigidbody2D rigidBody2D;

	public float runSpeed = 5;
	public float jumpSpeed = 200f;

	public SpriteRenderer spriteRenderer;
	public Animator animator;
	private int count;


	// Start is called before the first frame update
	void Start()
    {
		rigidBody2D = GetComponent<Rigidbody2D>();
		count = 0;
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.GetButtonDown("Jump"))
		{
			int levelMask = LayerMask.GetMask("Level");

            if(Physics2D.BoxCast(transform.position, new Vector2(1, .1f), 0f, Vector2.down, .01f, levelMask))
			{
				Jump();
			}
		}
    }

    void FixedUpdate()
	{
		float horizontalInput = Input.GetAxis("Horizontal");

		Vector2 direction = new Vector2(horizontalInput * runSpeed * Time.deltaTime, 0);

		rigidBody2D.AddForce(direction);

        //AddForce
        if (rigidBody2D.velocity.x > 0)
		{
			spriteRenderer.flipX = false;
		}
		else
		{
			spriteRenderer.flipX = true;
		}

        //Animation
        if (Mathf.Abs(horizontalInput) > 0f)
		{
			animator.SetBool("IsRunning", true);
		}
		else
		{
			animator.SetBool("IsRunning", false);
		}
	}

    void Jump()
	{
		rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, jumpSpeed);
	}


	private void OnCollisionEnter(Collider other)
	{
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);
			count += 1;

			//SetCountText();
		}
	}


	void SetCountText()
	{
		//countText.text = "Count: " + count.ToString();
	}

}